from django.test import Client, TestCase, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from selenium import webdriver
from homepage.apps import HomepageConfig
import unittest
import time

# Create your tests here.
class unitTest(TestCase):
    def test_home_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_signup_url_exist(self):
        response = Client().get('/signup')
        self.assertEqual(response.status_code,200)

#====================================================================================

    def test_home_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'home.html')

    def test_signup_template(self):
        response = Client().get('/signup')
        self.assertTemplateUsed(response, 'signup.html')

#====================================================================================

    def test_signup_form(self):
        response = self.client.post(reverse('homepage:signup'), data={'username': 'pecintamie', 'password1': 'mieayam123', 'password2': 'mieayam123'})
        count = User.objects.all().count()
        input = User.objects.all().first()
        self.assertEqual(input.username, 'pecintamie')
        self.assertEqual(count, 1)

#====================================================================================

class functionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox') 
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')

    def tearDown(self):
        self.driver.quit()

    def test_page_title(self):
        self.driver.get(self.live_server_url)
        self.assertIn("Story 10", self.driver.title)