from django.urls import include, path
from django.contrib import admin
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name = 'home'),
    path('signup', views.signup, name = 'signup')
]